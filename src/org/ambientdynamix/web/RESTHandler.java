/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.web;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Properties;

import org.ambientdynamix.api.application.ContextPluginInformationResult;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextSupportResult;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.core.WebFacadeBinder;
import org.ambientdynamix.web.NanoHTTPD.Response;

import android.os.Bundle;
import android.util.Log;

/**
 * Provides a REST interface for web clients connecting to Dynamix.
 * 
 * @author Darren Carlson
 * 
 */
public class RESTHandler {
	// Rest endpoints
	public static final String DYNAMIX_BIND = "/dynamixbind";
	public static final String DYNAMIX_UNBIND = "/dynamixunbind";
	public static final String DYNAMIX_VERSION = "/dynamixversion";
	public static final String EVENT_CALLBACK = "/eventcallback";
	public static final String ADD_DYNAMIX_LISTENER = "/adddynamixlistener";
	public static final String REMOVE_DYNAMIX_LISTENER = "/removedynamixlistener";
	public static final String ADD_CONTEXT_SUPPORT = "/addcontextsupport";
	public static final String ADD_PLUGIN_CONTEXT_SUPPORT = "/addplugincontextsupport";
	public static final String ADD_CONFIGURED_CONTEXT_SUPPORT = "/addconfiguredcontextsupport";
	public static final String REMOVE_CONTEXT_SUPPORT_FOR_TYPE = "/removecontextsupportforcontexttype";
	public static final String REMOVE_CONTEXT_SUPPORT_FOR_ID = "/removecontextsupportforsupportid";
	public static final String REMOVE_ALL_CONTEXT_SUPPORT = "/removeallcontextsupport";
	public static final String CONTEXT_REQUEST = "/contextrequest";
	public static final String CONFIGURED_CONTEXT_REQUEST = "/configuredcontextrequest";
	public static final String CHECK_DYNAMIX_ACTIVE = "/isdynamixactive";
	public static final String IS_DYNAMIX_TOKEN_VALID = "/isdynamixtokenvalid";
	public static final String IS_DYNAMIX_SESSION_OPEN = "/isdynamixsessionopen";
	public static final String GET_CONTEXT_SUPPORT = "/getcontextsupport";
	public static final String GET_ALL_CONTEXT_PLUG_INS = "/getallcontextplugininformation";
	public static final String GET_INSTALLED_CONTEXT_PLUG_INS = "/getinstalledcontextplugininformation";
	public static final String GET_CONTEXT_PLUG_IN = "/getcontextplugininformation";
	public static final String RESEND_CACHED_EVENTS = "/resendcachedcontextevents";
	public static final String OPEN_CONTEXT_PLUGIN_CONFIGURATION_VIEW = "/opencontextpluginconfigurationview";
	// Private data
	private WebFacadeBinder facade;
	private final String TAG = this.getClass().getSimpleName();

	/**
	 * Creates a RESTHandler.
	 * 
	 * @param facade
	 *            The WebFacadeBinder for this handler to use.
	 */
	public RESTHandler(WebFacadeBinder facade) {
		this.facade = facade;
	}

	/**
	 * Process a web client request using the Dynamix REST interface.
	 */
	public Response processRequest(Response r, WebListenerManager<String> wlMgr, String uri, String method,
			Properties header, Properties parms) throws Exception {
		// Start by setting the response to HTTP_OK with a MIME_HTML type and a '0' for the content (success)
		r.status = NanoHTTPD.HTTP_OK;
		r.mimeType = NanoHTTPD.MIME_HTML;
		r.setText("0");
		// Prep URI
		uri = uri.toLowerCase().trim();
		uri = URLDecoder.decode(uri);
		/*
		 * First, handle CONFIGURED_CONTEXT_REQUEST, which may be any REST method type.
		 */
		if (uri.startsWith(CONFIGURED_CONTEXT_REQUEST)) {
			/*
			 * Extract the portion of the URI that will be sent to the plug-in. In this case, we send the plug-in
			 * everything not related to the Dynamix call. The raw URI spec is
			 * http://localhost:PORT/<pluginId>/<contextType>/<customRestPath>. The query string and/or message body
			 * data will already be extracted into parms from NanoHTTPd
			 */
			/*
			 * Tokanize URI: [0] = <pluginId>; [1] = <contextType>; [2]..[n] = <customRestPath>
			 */
			String tmpUri;
			if (uri.startsWith("/"))
				tmpUri = uri.substring(1);
			else
				tmpUri = uri;
			String[] tokens = tmpUri.split("/");
			String pluginId;
			String contextType;
			String pluginUri = "/";
			// Validate tokens
			if (tokens.length >= 2) {
				// Setup plug-in id
				pluginId = tokens[1];
				// Setup context type
				contextType = tokens[2];
				// Setup custom rest path
				if (tokens.length > 2) {
					pluginUri = uri.substring(uri.indexOf(contextType) + contextType.length() + 1);
				}
				/*
				 * Handle the configuredContextRequest and return the result. During the call, we setup the config
				 * Bundle using the incoming Web data.
				 */
				parameterizeResponse(r, facade.configuredContextRequest(wlMgr.getListener(), pluginId, contextType,
						createConfiguredContextRequestBundle(method, pluginUri, header, parms)));
			} else {
				failOnMissingParam(r, "pluginId", "contextType");
			}
		}
		// Handle GET-based requests
		else if (method.equalsIgnoreCase("GET")) {
			// Handle EVENT_CALLBACK
			if (uri.startsWith(EVENT_CALLBACK)) {
				// Check if we need to wait for an event to send
				if (wlMgr.isEmpty()) {
					/*
					 * Wait for events to arrive; however, after 10 seconds we send HTTP_NOTFOUND so that the XHR
					 * request doesn't timeout. Javascript will then call us back to wait for events.
					 */
					wlMgr.waitForEvent(10000);
				}
				// Check for an event to send
				if (wlMgr.isEmpty()) {
					// No event, so send 404
					r.status = NanoHTTPD.HTTP_NOTFOUND;
				} else {
					// Access and send the event's command
					String command = wlMgr.poll();
					r.setText(command);
				}
			}
			// Handle CHECK_DYNAMIX_ACTIVE
			else if (uri.startsWith(CHECK_DYNAMIX_ACTIVE)) {
				r.setText(Boolean.toString(facade.isDynamixActive()));
			}
			// Handle ADD_DYNAMIX_LISTENER
			else if (uri.startsWith(ADD_DYNAMIX_LISTENER)) {
				facade.addDynamixListener(wlMgr.getListener());
			}
			// Handle REMOVE_DYNAMIX_LISTENER
			else if (uri.startsWith(REMOVE_DYNAMIX_LISTENER)) {
				facade.removeDynamixListener(wlMgr.getListener());
			}
			// Handle DYNAMIX_VERSION
			else if (uri.startsWith(DYNAMIX_VERSION)) {
				r.setText(facade.getDynamixVersion().toString());
			}
			// Handle RESEND_CACHED_EVENTS
			else if (uri.startsWith(RESEND_CACHED_EVENTS)) {
				String contextType = parms.getProperty("contextType");
				String pastMills = parms.getProperty("pastMills");
				if(pastMills != null && pastMills.equalsIgnoreCase("undefined"))
					pastMills = null;
				Integer pastMillsInt = null;
				// If we have pastMills, try to convert the string to an Integer
				if (pastMills != null) {
					try {
						pastMillsInt = Integer.parseInt(pastMills);
					} catch (Exception e) {
						Log.w(TAG, "Could not convert pastMills to integer: " + pastMills);
					}
				}
				if (contextType == null && pastMills == null) {
					parameterizeResponse(r, facade.resendAllCachedContextEvents((wlMgr.getListener())));
				} else if (contextType == null && pastMillsInt != null) {
					parameterizeResponse(r, facade.resendCachedContextEvents(wlMgr.getListener(), pastMillsInt));
				} else if (contextType != null && pastMills == null) {
					parameterizeResponse(r, facade.resendAllTypedCachedContextEvents(wlMgr.getListener(), contextType));
				} else if (contextType != null && pastMillsInt != null) {
					parameterizeResponse(r,
							facade.resendTypedCachedContextEvents(wlMgr.getListener(), contextType, pastMillsInt));
				} else {
					failOnMissingParam(r, "contextType");
				}
			}
			// Handle OPEN_CONTEXT_PLUGIN_CONFIGURATION_VIEW
			else if (uri.startsWith(OPEN_CONTEXT_PLUGIN_CONFIGURATION_VIEW)) {
				String pluginId = parms.getProperty("pluginId");
				if (pluginId != null)
					parameterizeResponse(r, facade.openContextPluginConfigurationView(wlMgr.getListener(), pluginId));
				else
					failOnMissingParam(r, "pluginId");
			}
			// Handle GET_CONTEXT_SUPPORT
			else if (uri.startsWith(GET_CONTEXT_SUPPORT)) {
				parameterizeContextSupportResultResponse(r, facade.getContextSupport(wlMgr.getListener()));
			}
			// Handle GET_ALL_CONTEXT_PLUG_INS
			else if (uri.startsWith(GET_ALL_CONTEXT_PLUG_INS)) {
				parameterizeContextPluginInformationResultResponse(r,
						facade.getContextPluginInformation(wlMgr.getListener(), true));
			}
			// Handle GET_INSTALLED_CONTEXT_PLUG_INS
			else if (uri.startsWith(GET_INSTALLED_CONTEXT_PLUG_INS)) {
				parameterizeContextPluginInformationResultResponse(r,
						facade.getContextPluginInformation(wlMgr.getListener(), false));
			}
			// Handle GET_CONTEXT_PLUG_IN
			else if (uri.startsWith(GET_CONTEXT_PLUG_IN)) {
				String pluginId = parms.getProperty("pluginId");
				if (pluginId != null) {
					parameterizeContextPluginInformationResultResponse(r,
							facade.getContextPluginInformation(wlMgr.getListener(), pluginId));
				} else {
					failOnMissingParam(r, "pluginId");
				}
			}
			// Handle ADD_CONTEXT_SUPPORT
			else if (uri.startsWith(ADD_CONTEXT_SUPPORT)) {
				String contextType = parms.getProperty("contextType");
				if (contextType != null) {
					parameterizeResponse(r, facade.addContextSupport(wlMgr.getListener(), contextType));
				} else {
					failOnMissingParam(r, "contextType");
				}
			}
			// Handle ADD_PLUGIN_CONTEXT_SUPPORT
			else if (uri.startsWith(ADD_PLUGIN_CONTEXT_SUPPORT)) {
				String pluginId = parms.getProperty("pluginId");
				String contextType = parms.getProperty("contextType");
				if (pluginId != null && contextType != null) {
					parameterizeResponse(r, facade.addPluginContextSupport(wlMgr.getListener(), pluginId, contextType));
				} else {
					failOnMissingParam(r, "pluginId", "contextType");
				}
			}
			// Handle ADD_CONFIGURED_CONTEXT_SUPPORT
			else if (uri.startsWith(ADD_CONFIGURED_CONTEXT_SUPPORT)) {
				// Not supported for Web clients
				r.status = NanoHTTPD.HTTP_NOTIMPLEMENTED;
				r.setText("ADD_CONFIGURED_CONTEXT_SUPPORT not supported, use ADD_CONTEXT_SUPPORT");
			}
			// Handle REMOVE_CONTEXT_SUPPORT_FOR_TYPE
			else if (uri.startsWith(REMOVE_CONTEXT_SUPPORT_FOR_TYPE)) {
				String contextType = parms.getProperty("contextType");
				if (contextType != null) {
					parameterizeResponse(r, facade.removeContextSupportForContextType(wlMgr.getListener(), contextType));
				} else {
					failOnMissingParam(r, "contextType");
				}
			}
			// Handle REMOVE_CONTEXT_SUPPORT_FOR_ID
			else if (uri.startsWith(REMOVE_CONTEXT_SUPPORT_FOR_ID)) {
				String supportId = parms.getProperty("supportId");
				/*
				 * Create a ContextSupportInfo based on the supportId. Note that we can use nulls for the plug-in info
				 * and context type, since 'removeContextSupport' searches using the supportId only.
				 */
				ContextSupportInfo supportInfo = new ContextSupportInfo(supportId, null, null);
				if (supportId != null) {
					parameterizeResponse(r, facade.removeContextSupport(wlMgr.getListener(), supportInfo));
				} else {
					failOnMissingParam(r, "supportId");
				}
			}
			// Handle REMOVE_ALL_CONTEXT_SUPPORT
			else if (uri.startsWith(REMOVE_ALL_CONTEXT_SUPPORT)) {
				parameterizeResponse(r, facade.removeAllContextSupportForListener(wlMgr.getListener()));
			}
			// Handle CONTEXT_REQUEST
			else if (uri.startsWith(CONTEXT_REQUEST)) {
				String pluginId = parms.getProperty("pluginId");
				String contextType = parms.getProperty("contextType");
				if (pluginId != null && contextType != null)
					parameterizeResponse(r, facade.contextRequest(wlMgr.getListener(), pluginId, contextType));
				else {
					failOnMissingParam(r, "pluginId", "contextType");
				}
			}
			// Error: No valid REST endpoint found
			else {
				Log.w(TAG, "REST endpoint invalid: " + uri);
				r.status = NanoHTTPD.HTTP_BADREQUEST;
				r.setText("REST endpoint invalid: " + uri);
			}
		}
		// Invalid: Request was not CONFIGURED_CONTEXT_REQUEST or GET
		else {
			Log.w(TAG, "Invalid request: " + uri);
			r.status = NanoHTTPD.HTTP_BADREQUEST;
			r.setText("Invalid request: " + uri);
		}
		return r;
	}

	/*
	 * Helper method that creates a configuration Bundle for context requests.
	 */
	private Bundle createConfiguredContextRequestBundle(String method, String uri, Properties header, Properties parms)
			throws Exception {
		// Create a Bundle to hold the request config
		Bundle b = new Bundle();
		// Set WEB_REQUEST to true
		b.putBoolean(PluginConstants.WEB_REQUEST, true);
		// Add the WEB_REQUEST_METHOD method
		b.putString(PluginConstants.WEB_REQUEST_METHOD, method);
		// Add the WEB_REQUEST_URI
		b.putString(PluginConstants.WEB_REQUEST_URI, uri);
		// Add the WEB_REQUEST_CONTENT_TYPE
		b.putString(PluginConstants.WEB_REQUEST_CONTENT_TYPE, header.getProperty("content-type"));
		// Add the WEB_REQUEST_URI_PROPERTIES
		b.putSerializable(PluginConstants.WEB_REQUEST_URI_PROPERTIES, parms);
		// For convenience, extract any query string parameters into a request Bundle
		// Remove initial: '/?' or '?'
		if (uri.startsWith("/?"))
			uri = uri.substring(2);
		else if (uri.startsWith("/"))
			uri = uri.substring(1);
		else if (uri.startsWith("?"))
			uri = uri.substring(1);
		// Split parameter pairs
		String[] nvPairs = uri.split("&");
		// Inject up each individual parameter pair into the config Bundle
		for (String rawPair : nvPairs) {
			String[] pair = rawPair.split("=");
			if (pair.length == 2) {
				// make sure the string can't overwrite Dynamix Bundle keys
				String name = pair[0];
				if (!name.equalsIgnoreCase(PluginConstants.WEB_REQUEST)
						&& !name.equalsIgnoreCase(PluginConstants.WEB_REQUEST_METHOD)
						&& !name.equalsIgnoreCase(PluginConstants.WEB_REQUEST_URI)
						&& !name.equalsIgnoreCase(PluginConstants.WEB_REQUEST_CONTENT_TYPE)
						&& !name.equalsIgnoreCase(PluginConstants.WEB_REQUEST_URI_PROPERTIES)) {
					b.putString(name, pair[1]);
				} else {
					throw new Exception("Query string parameters cannot overwrite Dynamix keys");
				}
			} else {
				throw new Exception("Malformed query string. Problem near " + rawPair);
			}
		}
		return b;
	}

	/*
	 * Helper method for parameterizing a Response with a Result.
	 */
	private void parameterizeResponse(Response response, Result result) {
		if (result.wasSuccessful()) {
			response.setText(ErrorCodes.NO_ERROR + ",SUCCESS");
		} else {
			response.setText(result.getErrorCode() + "," + result.getMessage());
		}
	}

	/*
	 * Helper method for parameterizing a Response with an IdResult.
	 */
	private void parameterizeResponse(Response response, IdResult result) {
		if (result.wasSuccessful()) {
			response.setText(ErrorCodes.NO_ERROR + "," + result.getId());
		} else {
			response.setText(result.getErrorCode() + "," + result.getMessage());
		}
	}

	/*
	 * Helper method for parameterizing a Response with a ContextSupportResult.
	 */
	private void parameterizeContextSupportResultResponse(Response response, ContextSupportResult result) {
		if (result.wasSuccessful()) {
			response.mimeType = NanoHTTPD.MIME_HTML;
			try {
				String json = WebUtils.serializeObject(result.getContextSupportInfo());
				response.setText(ErrorCodes.NO_ERROR + "," + URLEncoder.encode(json));
			} catch (Exception e) {
				Log.w(TAG, "Could not encode plug-ins: " + e);
				response.setText(ErrorCodes.DYNAMIX_FRAMEWORK_ERROR + "," + e.toString());
			}
		} else {
			response.setText(result.getErrorCode() + "," + result.getMessage());
		}
	}

	/*
	 * Helper method for parameterizing a Response with a ContextPluginInformationResult.
	 */
	private void parameterizeContextPluginInformationResultResponse(Response response,
			ContextPluginInformationResult result) {
		if (result.wasSuccessful()) {
			response.mimeType = NanoHTTPD.MIME_HTML;
			try {
				String json = WebUtils.serializeObject(result.getContextPluginInformation());
				response.setText(ErrorCodes.NO_ERROR + "," + URLEncoder.encode(json));
			} catch (Exception e) {
				Log.w(TAG, "Could not encode plug-ins: " + e);
				response.setText(ErrorCodes.DYNAMIX_FRAMEWORK_ERROR + "," + e.toString());
			}
		} else {
			response.setText(result.getErrorCode() + "," + result.getMessage());
		}
	}

	/*
	 * Utility that configures a failed response for requests that are missing parameters.
	 */
	private void failOnMissingParam(Response r, String... missingParams) {
		StringBuilder params = new StringBuilder();
		for (String param : missingParams) {
			params.append(param + " ");
		}
		Log.w(TAG, "Request missing parameters. Required Parameters: " + params.toString());
		r.setText(ErrorCodes.MISSING_PARAMETERS + ",Required Parameters: " + params.toString());
	}
}
