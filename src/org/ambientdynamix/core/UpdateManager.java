/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.ambientdynamix.api.application.VersionInfo;
import org.ambientdynamix.api.contextplugin.ContextPlugin;
import org.ambientdynamix.api.contextplugin.DynamixFeatureInfo;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.data.DynamixPreferences;
import org.ambientdynamix.update.DynamixUpdates;
import org.ambientdynamix.update.DynamixUpdatesBinder;
import org.ambientdynamix.update.contextplugin.ContextPluginConnectorFactory;
import org.ambientdynamix.update.contextplugin.IContextPluginConnector;
import org.ambientdynamix.update.contextplugin.PendingContextPlugin;
import org.ambientdynamix.util.RepositoryInfo;
import org.ambientdynamix.util.Utils;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import android.content.Context;
import android.content.pm.FeatureInfo;
import android.os.Environment;
import android.util.Log;

/**
 * Manages update discovery and notification for the Dynamix Framework. Currently, this class only discovers updates for
 * ContextPlugins; however, long term, all dynamically updatable parts of the framework will be managed through this
 * class.
 * <p>
 * ContextPlugin XML descriptions must adhere to the specification as described in the Dynamix developer documentation.
 * The class will automatically use a backup server (if provided) if access to the primary update server is fails
 * (automatic failover).
 * 
 * @author Darren Carlson
 */
class UpdateManager {
	// Private data
	private static final String TAG = UpdateManager.class.getSimpleName();
	private static IContextPluginConnector currentPlugConnector;
	private static volatile boolean cancelled;
	private static volatile boolean processingContextPluginUpdates;

	// Singleton constructor
	private UpdateManager() {
	}

	/**
	 * Checks for Dynamix Framework updates.
	 */
	public static synchronized void checkForDynamixFrameworkUpdates(final Context c, final String updateUrl,
			final IDynamixUpdateListener listener, boolean runAsynchronously) {
		if (!Utils.isPublicNetworkAccessAllowed(true)) {
			if (listener != null)
				listener.onUpdateCancelled();
		} else
			Utils.dispatch(runAsynchronously, new Runnable() {
				@Override
				public void run() {
					Log.i(TAG, "Starting Dynamix Framework Update....");
					listener.onUpdateStarted();
					try {
						URL server = new URL(updateUrl);
						InputStream input = server.openStream();
						Serializer serializer = new Persister();
						SAXReader reader = new SAXReader(); // dom4j SAXReader
						/*
						 * TODO: Using the dom4j Document here, since it can load input from a variety of sources
						 * automatically (file and network). We could explore providing our own low-overhead transport
						 * mechanisms, if the Document gets too heavy.
						 */
						Document document = reader.read(input);
						String xml = document.asXML();
						input.close();
						Reader metaReader = new StringReader(xml);
						DynamixUpdatesBinder updatesBinder = serializer.read(DynamixUpdatesBinder.class, metaReader,
								false);
						DynamixUpdates updates = new DynamixUpdates();
						updates.setTrustedWebConnectorCerts(updatesBinder.getTrustedWebConnectorCerts());
						listener.onUpdateComplete(updates);
					} catch (Exception e) {
						Log.w(TAG, "Dynamix Update Failed: " + e);
						listener.onUpdateError(e.getMessage());
					}
				}
			});
	}

	public static PendingContextPlugin getPendingContextPlugin(String metadataUri) throws URISyntaxException {
		URI uri = new URI(metadataUri);
		
		return null;
	}

	/**
	 * Cancels an existing update operation initiated by 'checkForContextPluginUpdates'.
	 */
	public static void cancelContextPluginUpdate() {
		Log.d(TAG, "cancelContextPluginUpdate");
		cancelled = true;
		if (currentPlugConnector != null) {
			currentPlugConnector.cancel();
		}
		currentPlugConnector = null;
	}

	/**
	 * Asynchronously checks for context plugin updates using the incoming server(s), notifying the specified
	 * IUpdateStatusListener with results (or errors).
	 * 
	 * @param plugRepos
	 *            The List of IContextPluginConnector entities to check for updates
	 * @param platform
	 *            The device platform
	 * @param platformVersion
	 *            The device platform version
	 * @param frameworkVersion
	 *            The Dynamix version
	 * @param handler
	 *            The IUpdateStatusListener to notify with results (or errors)
	 */
	public static synchronized void checkForContextPluginUpdates(final Context c, final List<RepositoryInfo> plugRepos,
			final PluginConstants.PLATFORM platform, final VersionInfo platformVersion,
			final VersionInfo frameworkVersion, final IContextPluginUpdateListener callback,
			final FeatureInfo[] availableFeatures, boolean runAsynchronously) {
		if (!processingContextPluginUpdates) {
			Log.d(TAG, "Checking for plug-in updates with source count: " + plugRepos.size());
			// Handle notifications
			SessionManager.notifyAllContextPluginDiscoveryStarted();
			// Setup state
			processingContextPluginUpdates = true;
			cancelled = false;
			Utils.dispatch(runAsynchronously, new Runnable() {
				@Override
				public void run() {
					Log.d(TAG, "Started checking for contect plug-in updates...");
					List<PendingContextPlugin> existingPlugs = DynamixService.getPendingContextPlugins();
					// Stop the Dynamix update timer
					DynamixService.stopContextPluginUpdateTimer();
					// Notify the callback that we've started the update
					if (callback != null) {
						Utils.dispatch(true, new Runnable() {
							@Override
							public void run() {
								callback.onUpdateStarted();
							}
						});
					}
					List<PendingContextPlugin> updates = new Vector<PendingContextPlugin>();
					final Map<IContextPluginConnector, String> errors = new HashMap<IContextPluginConnector, String>();
					for (RepositoryInfo repo : plugRepos) {
						if (cancelled) {
							Log.w(TAG, "Exiting checkForContextPluginUpdates with finished: " + cancelled);
							break;
						}
						// Check if we're allowed to use the network
						if (!Utils.isPublicNetworkAccessAllowed(true) && repo.isNetworkSource()) {
							Log.w(TAG, "Skipping update since WIFI is not enabled for: " + repo);
							/*
							 * Since we're not allowed to access the network, just reload what we've already found for
							 * the source
							 */
							for (PendingContextPlugin discovered : existingPlugs) {
								if (discovered.getPendingContextPlugin().getRepoSource().equals(repo))
									updates.add(discovered);
							}
						} else {
							try {
								currentPlugConnector = ContextPluginConnectorFactory.makeContextPluginConnector(repo);
								Date lastModifiedLocal = DynamixService.getRepoLastModified(currentPlugConnector
										.getConnectorId());
								if (lastModifiedLocal == null)
									lastModifiedLocal = new Date();
								Date lastModifiedRemote = currentPlugConnector.getLastModified();
								try {
									Log.d(TAG, "lastModifiedLocal " + lastModifiedLocal.toGMTString()
											+ " lastModifiedRemote " + lastModifiedRemote.toGMTString());
									// if (lastModifiedRemote.getTime() > lastModifiedLocal.getTime()) {
									/*
									 * TODO: We should get the last modified check working properly. Currently, we
									 * always check for updates, regardless of last modified.
									 */
									if (true) {
										// Local information is stale... do the update
										Log.d(TAG, "Repo is modified: " + currentPlugConnector.getConnectorId());
										Log.i(TAG, "Updating from PluginSource: " + currentPlugConnector);
										List<PendingContextPlugin> potentialUpdates = currentPlugConnector
												.getContextPlugins(platform, platformVersion, frameworkVersion);
										List<PendingContextPlugin> remove = new ArrayList<PendingContextPlugin>();
										// Scan the update list and remove problem plug-ins
										for (PendingContextPlugin update : potentialUpdates) {
											ContextPlugin plug = update.getPendingContextPlugin();
											// Check for update error
											if (update.hasError()) {
												errors.put(currentPlugConnector, update.getErrorMessage());
												remove.add(update);
												break;
											}
											// Check for plug-in validation errors
											if (!Utils.validateContextPlugin(plug)) {
												errors.put(currentPlugConnector, "Plug-in validation error");
												remove.add(update);
												break;
											}
											// Check for framework dependencies
											if (plug.hasMaxFrameworkVersion()) {
												if (DynamixService.getDynamixFrameworkVersion().compareTo(
														plug.getMaxFrameworkVersion()) >= 0) {
													Log.d(TAG, "Removing framework incompatible plug: " + plug);
													remove.add(update);
												}
											}
											// Check for feature dependencies
											if (plug.hasFeatureDependencies()) {
												for (DynamixFeatureInfo featureDependency : plug
														.getFeatureDependencies()) {
													if (featureDependency != null && featureDependency.isRequired()) {
														boolean featureFound = false;
														for (FeatureInfo feature : availableFeatures) {
															if (feature.name != null
																	&& feature.name.equalsIgnoreCase(featureDependency
																			.getName())) {
																featureFound = true;
																break;
															}
														}
														// If we didn't find a required feature, remove the
														// ContextPlugin
														if (!featureFound) {
															Log.d(TAG, "Removing feature incompatible plug: " + plug);
															remove.add(update);
														}
													}
												}
											}
										}
										// Remove problematic updates
										potentialUpdates.removeAll(remove);
										// Finally, add all compatible updates.
										updates.addAll(potentialUpdates);
										// Set the last modified time for this repo
										DynamixService.setRepoLastModified(currentPlugConnector.getConnectorId(),
												lastModifiedRemote);
									} else {
										// Local info is up-to-date... re-add what we've already found
										Log.d(TAG, "Repo is unmodified: " + currentPlugConnector.getConnectorId());
										for (PendingContextPlugin plug : DynamixService.getPendingContextPlugins()) {
											if (plug.getPendingContextPlugin().getRepoSource().getUrl()
													.equalsIgnoreCase(currentPlugConnector.getConnectorId()))
												updates.add(plug);
										}
									}
								} catch (Exception e) {
									Log.w(TAG, e);
									errors.put(currentPlugConnector, e.toString());
								}
								currentPlugConnector = null;
							} catch (Exception e) {
								Log.w(TAG, "Exception during update: " + e.toString());
							}
						}
					}
					if (cancelled && callback != null) {
						Utils.dispatch(true, new Runnable() {
							@Override
							public void run() {
								callback.onUpdateCancelled();
							}
						});
					} else {
						final List<PendingContextPlugin> finalResults = updates;
						if (callback != null) {
							Utils.dispatch(true, new Runnable() {
								@Override
								public void run() {
									callback.onUpdateComplete(finalResults, errors);
								}
							});
						}
					}
					Log.d(TAG, "Completed checking for context plug-in updates");
					// Restart the update timer, if necessary
					DynamixService.startContextPluginUpdateTimer();
					DynamixService.updateNotifications();
					processingContextPluginUpdates = false;
				}
			});
		} else
			Log.w(TAG, "Already discovering plug-ins!");
	}

	/**
	 * Returns the list of available RepositoryInfo(s), which are used for discovering new plug-ins and plug-in updates.
	 */
	static List<RepositoryInfo> getContextPluginRepos() {
		List<RepositoryInfo> sources = new Vector<RepositoryInfo>();
		// Setup primary Dynamix context plug-in repository
		if (DynamixPreferences.isDynamixRepositoryEnabled(DynamixService.getAndroidContext())) {
			RepositoryInfo repo = DynamixService.getConfig().getPrimaryContextPluginRepo();
			if (repo != null) {
				if (!DynamixService.isEmbedded())
					repo.setUrl(DynamixPreferences.getNetworkContextPluginDiscoveryPath(DynamixService
							.getAndroidContext()));
				try {
					sources.add(repo);
				} catch (Exception e) {
					Log.e(TAG, "Could not make repository using: " + repo);
				}
			}
		}
		// Setup local Dynamix context plug-in repository (if enabled)
		if (DynamixPreferences.localContextPluginDiscoveryEnabled(DynamixService.getAndroidContext())) {
			RepositoryInfo repo = null;
			if (DynamixService.isEmbedded()) {
				repo = DynamixService.getConfig().getLocalPluginRepo();
				if (repo != null) {
					String baseUrl = repo.getUrl();
					String storageDirectory = Environment.getExternalStorageDirectory().getAbsolutePath();
					if (repo.getUrl() != null) {
						// Setup storage directory, if needed
						if (!repo.getUrl().contains(storageDirectory))
							repo.setUrl(Environment.getExternalStorageDirectory().getAbsolutePath() + baseUrl);
					} else
						Log.w(TAG, "URL was null for: " + repo);
				}
			} else {
				repo = new RepositoryInfo("Local Context Plug-in Repo", null, RepositoryInfo.SIMPLE_FILE_SOURCE);
				repo.setUrl(Environment.getExternalStorageDirectory().getAbsolutePath()
						+ DynamixPreferences.getLocalContextPluginDiscoveryPath(DynamixService.getAndroidContext()));
			}
			if (repo != null) {
				try {
					sources.add(repo);
				} catch (Exception e) {
					Log.e(TAG, "Could not make repository using: " + repo);
				}
			}
		}
		// Setup 3rd party Dynamix context plug-in repository (if enabled)
		if (DynamixPreferences.isExternalRepositoryEnabled(DynamixService.getAndroidContext())) {
			RepositoryInfo repo = null;
			if (DynamixService.isEmbedded()) {
				repo = DynamixService.getConfig().getExternalPluginRepo();
			} else {
				repo = new RepositoryInfo();
				repo.setAlias("3rd Party Repository");
				repo.setUrl(DynamixPreferences.getExternalDiscoveryPath(DynamixService.getAndroidContext()));
				repo.setType(RepositoryInfo.SIMPLE_NETWORK_SOURCE);
			}
			if (repo != null) {
				try {
					sources.add(repo);
				} catch (Exception e) {
					Log.e(TAG, "Could not make repository using: " + repo);
				}
			}
		}
		// TODO: Dynamix Nexus Repo test (remove)
		// RepositoryInfo repo = new RepositoryInfo(
		// "Dynamix Core Context Plug-in Repo",
		// "http://repo1.ambientdynamix.org:8081/nexus/service/local/data_index?g=org.ambientdynamix.contextplugins&repositoryId=dynamix-core-contextplugins",
		// ContextPluginConnectorFactory.NEXUS_INDEX_SOURCE);
		// RepositoryInfo repo = new RepositoryInfo(
		// "Dynamix Core Context Plug-in Repo",
		// "http://repo1.ambientdynamix.org:8081/nexus/service/local/lucene/search?g=org.ambientdynamix.contextplugins&repositoryId=dynamix-core-contextplugins",
		// ContextPluginConnectorFactory.NEXUS_LUCENE_SOURCE);
		// try {
		// sources.add(ContextPluginConnectorFactory.makeContextPluginConnector(repo));
		// } catch (Exception e) {
		// Log.e(TAG, "Could not make repository using: " + repo);
		// }
		return sources;
	}

	/**
	 * Returns a List of PendingContextPlugin that cane be used to update existing installed plug-ins.
	 */
	static List<PendingContextPlugin> getFilteredContextPluginUpdates() {
		List<PendingContextPlugin> results = new Vector<PendingContextPlugin>();
		List<PendingContextPlugin> filtered = UpdateManager.filterDiscoveredPlugins();
		for (PendingContextPlugin pending : filtered) {
			if (pending.isUpdate())
				results.add(pending);
		}
		return results;
	}

	/**
	 * Returns a List of PendingContextPlugin that are not yet installed.
	 */
	static List<PendingContextPlugin> getNewContextPlugins() {
		List<PendingContextPlugin> results = new Vector<PendingContextPlugin>();
		List<PendingContextPlugin> filtered = UpdateManager.filterDiscoveredPlugins();
		for (PendingContextPlugin pending : filtered) {
			if (!pending.isUpdate())
				results.add(pending);
		}
		return results;
	}

	/**
	 * Returns a List of PendingContextPlugins that are either newly discovered or are pending updates to already
	 * installed plug-ins. Note that this method requires that 'checkForContextPluginUpdates' has already successfully
	 * stored a list of context plug-in updates in the SettingsManager.
	 */
	static List<PendingContextPlugin> filterDiscoveredPlugins() {
		// Create a list of updates to return after filtering
		List<PendingContextPlugin> results = new Vector<PendingContextPlugin>();
		// Grab and scan through the list of pending plug-ins
		List<PendingContextPlugin> pendingPlugs = DynamixService.SettingsManager.getPendingContextPlugins();
		for (PendingContextPlugin pendingPlugin : pendingPlugs) {
			if (pendingPlugin != null) {
				boolean found = false;
				// Create a cloned PendingContextPlugin (so that database state is maintained)
				PendingContextPlugin updateClone = new PendingContextPlugin(pendingPlugin.getPendingContextPlugin()
						.clone(), pendingPlugin.getUpdateMessage(), pendingPlugin.getPriority());
				ContextPlugin clonePlug = updateClone.getPendingContextPlugin();
				// Scan through the list of existing context plugins, looking for a plugin associated with the update
				for (ContextPlugin target : DynamixService.SettingsManager.getInstalledContextPlugins()) {
					// Check for an ID match
					if (target.getId().equalsIgnoreCase(clonePlug.getId())) {
						found = true;
						// We have a matching ID. Now check if the new plugin's
						// version is greater than the existing's
						if (clonePlug.getVersionInfo().compareTo(target.getVersionInfo()) > 0) {
							// The plugin listed in the update is newer... list it as the target of the update
							updateClone.setUpdateTarget(target);
							results.add(updateClone);
							// Log.d(TAG, clonePlug + " can be used to update " + target);
						} else
							// Log.d(TAG, "Currently installed ContextPlugin " + target + " is >= " + clonePlug);
							break;
					}
				}
				// If a target plugin was not found, add it as new
				if (!found) {
					results.add(updateClone);
				}
			} else
				Log.e(TAG, "SettingsManager contained a NULL PendingContextPlugin");
		}
		return results;
	}

	/**
	 * Base interface for update listeners
	 * 
	 * @author Darren Carlson
	 */
	interface IBaseUpdateListener {
		/**
		 * Raised when the update is started.
		 */
		void onUpdateStarted();

		/**
		 * Raised if the update is cancelled.
		 */
		void onUpdateCancelled();

		/**
		 * Raised if there was an update error.
		 */
		void onUpdateError(String message);
	}

	/**
	 * Interface for listeners interested in receiving updates about plug-in updates.
	 * 
	 * @author Darren Carlson
	 */
	interface IContextPluginUpdateListener extends IBaseUpdateListener {
		/**
		 * Raised when the update is complete. Provides a list of UpdateResults and (possibly) a Map of
		 * IContextPluginConnector error messages.
		 */
		void onUpdateComplete(List<PendingContextPlugin> incomingUpdates, Map<IContextPluginConnector, String> errors);
	}

	/**
	 * Interface for listeners interested in receiving updates about Dynamix updates.
	 * 
	 * @author Darren Carlson
	 */
	interface IDynamixUpdateListener extends IBaseUpdateListener {
		void onUpdateComplete(DynamixUpdates updates);
	}
}