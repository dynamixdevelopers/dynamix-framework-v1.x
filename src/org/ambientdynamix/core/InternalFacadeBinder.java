/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.core;

import java.net.MalformedURLException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextSupportResult;
import org.ambientdynamix.api.application.IDynamixListener;
import org.ambientdynamix.api.application.IdResult;
import org.ambientdynamix.api.application.Result;
import org.ambientdynamix.api.contextplugin.ContextPlugin;
import org.ambientdynamix.core.DynamixApplication.APP_TYPE;
import org.ambientdynamix.util.Utils;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

/**
 * Every 'internal class' (i.e., plug-in or Dynamix component) wishing to request context support will be given a
 * customized version of the InternalFacadeBinder.
 * 
 * @author Darren Carlson
 * 
 */
public class InternalFacadeBinder extends AppFacadeBinder {
	private String TAG = this.getClass().getSimpleName();
	private UUID securityToken;
	private ContextPlugin plug;
	private boolean active = true;
	private APP_TYPE type;
	private String appId;
	private Map<String, InternalDynamixListener> mappedListeners = new ConcurrentHashMap<String, InternalDynamixListener>();

	public UUID getSecurityToken() {
		return this.securityToken;
	}

	public ContextPlugin getContextPlugin() {
		return plug;
	}

	public InternalFacadeBinder(APP_TYPE type, ContextPlugin plug, UUID securityToken, Context context,
			ContextManager conMgr, boolean embeddedMode) {
		super(context, conMgr, embeddedMode, false);
		this.plug = plug;
		this.securityToken = securityToken;
		this.type = type;
		this.appId = Utils.makeAppId(type, plug.getId());
		/*
		 * We need to add the framework listener AFTER the super constructor above, since we need an appId before
		 * calling 'DynamixService.addDynamixFrameworkListener', which uses our 'equals', which requires an appId. Whew!
		 */
		DynamixService.addDynamixFrameworkListener(this);
		// Log.d(TAG, "Created InternalFacadeBinder");
	}

	public String getAppId() {
		return this.appId;
	}

	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	private void verifyActive() throws RemoteException {
		if (!isActive())
			throw new RemoteException();
	}

	@Override
	public void addDynamixListener(IDynamixListener listener) throws RemoteException {
		verifyActive();
		super.addDynamixListener(mapListener(listener));
		// We automatically open a session, if needed
		if (!isSessionOpen())
			openSession();
	}

	@Override
	public void removeDynamixListener(IDynamixListener listener) throws RemoteException {
		super.removeDynamixListener(mapListener(listener));
		synchronized (mappedListeners) {
			InternalDynamixListener tmp = mappedListeners.remove(mapListener(listener));
			Log.d(TAG, "Removing " + tmp + " which was mapped to " + listener);
		}
	}

	@Override
	public IdResult addContextSupport(IDynamixListener listener, String contextType) throws RemoteException {
		verifyActive();
		/*
		 * Do not use mapListener here, since the super method will call 'addPluginContextSupport', which will do the
		 * mapping. If we map here, the second call to map won't work properly.
		 */
		return super.addContextSupport(listener, contextType);
	}

	@Override
	public IdResult addConfiguredContextSupport(IDynamixListener listener, Bundle contextConfiguration)
			throws RemoteException {
		verifyActive();
		return super.addConfiguredContextSupport(mapListener(listener), contextConfiguration);
	}

	@Override
	public IdResult addPluginContextSupport(IDynamixListener listener, String pluginId, String contextType)
			throws RemoteException {
		verifyActive();
		return super.addPluginContextSupport(mapListener(listener), pluginId, contextType);
	}

	@Override
	public IdResult configuredContextRequest(IDynamixListener listener, String pluginId, String contextType,
			Bundle requestConfig) throws RemoteException {
		verifyActive();
		return super.configuredContextRequest(mapListener(listener), pluginId, contextType, requestConfig);
	}

	@Override
	public IdResult contextRequest(IDynamixListener listener, String pluginId, String contextType)
			throws RemoteException {
		verifyActive();
		/*
		 * Do not use mapListener here, since the super method will call 'configuredContextRequest', which will do the
		 * mapping. If we map here, the second call to map won't work properly.
		 */
		return super.contextRequest(listener, pluginId, contextType);
	}

	@Override
	public ContextSupportResult getContextSupport(IDynamixListener listener) throws RemoteException {
		verifyActive();
		return super.getContextSupport(mapListener(listener));
	}

	@Override
	public IdResult getListenerId(IDynamixListener listener) throws RemoteException {
		verifyActive();
		return super.getListenerId(mapListener(listener));
	}

	@Override
	public Result removeAllContextSupportForListener(IDynamixListener listener) throws RemoteException {
		verifyActive();
		return super.removeAllContextSupportForListener(mapListener(listener));
	}

	@Override
	public Result removeContextSupport(IDynamixListener listener, ContextSupportInfo supportInfo)
			throws RemoteException {
		verifyActive();
		return super.removeContextSupport(mapListener(listener), supportInfo);
	}

	@Override
	public Result removeContextSupportForContextType(IDynamixListener listener, String contextType)
			throws RemoteException {
		verifyActive();
		return super.removeContextSupportForContextType(mapListener(listener), contextType);
	}

	@Override
	public Result resendTypedCachedContextEvents(IDynamixListener listener, String contextType, int pastMills)
			throws RemoteException {
		verifyActive();
		return super.resendTypedCachedContextEvents(mapListener(listener), contextType, pastMills);
	}

	@Override
	public Result resendCachedContextEvents(IDynamixListener listener, int pastMills) throws RemoteException {
		verifyActive();
		return super.resendCachedContextEvents(mapListener(listener), pastMills);
	}

	@Override
	public Result resendAllTypedCachedContextEvents(IDynamixListener listener, String contextType)
			throws RemoteException {
		verifyActive();
		return super.resendAllTypedCachedContextEvents(mapListener(listener), contextType);
	}

	@Override
	public Result resendAllCachedContextEvents(IDynamixListener listener) throws RemoteException {
		verifyActive();
		return super.resendAllCachedContextEvents(mapListener(listener));
	}

	private String getListenerSystemId(IDynamixListener listener) {
		String id = appId + ":" + listener.getClass().getName() + ":" + System.identityHashCode(listener);
		// Log.d(TAG, listener + " has system id " + id);
		return id;
	}

	/*
	 * Maps the incoming IDynamixListener
	 */
	private IDynamixListener mapListener(IDynamixListener incoming) throws RemoteException {
		// Log.v(TAG, "mapListener: Incoming listener " + incoming + " with identity " + getListenerSystemId(incoming));
		/*
		 * The listener should be a binder, not an InternalDynamixListener We're simply mapping the binder to the
		 * internal listener
		 */
		if (incoming instanceof InternalDynamixListener) {
			Log.w(TAG, "mapListener: " + incoming + " is an InternalDynamixListener... BAD, BAD, BAD!!!");
			try {
				throw new RuntimeException();
			} catch (Exception e) {
				Log.e(TAG, e.toString());
				e.printStackTrace();
			}
		}
		if (incoming != null) {
			synchronized (mappedListeners) {
				if (mappedListeners.containsKey(getListenerSystemId(incoming))) {
					InternalDynamixListener mapped = mappedListeners.get(getListenerSystemId(incoming));
					// Log.d(TAG, "mapListener: " + incoming + " was already mapped to " + mapped);
					return mapped;
				} else {
					InternalDynamixListener mapped = new InternalDynamixListener(this.appId, incoming);
					mappedListeners.put(getListenerSystemId(incoming), mapped);
					// Log.d(TAG, "mapListener: mapping " + incoming + " to " + mapped);
					return mapped;
				}
			}
		} else
			throw new RemoteException();
	}

	@Override
	protected String getCallerId(IDynamixListener listener) {
		return appId;
	}

	@Override
	/**
	 * Creates a new application using the caller's unique UID from Android.
	 */
	protected DynamixApplication createNewApplicationFromCaller(String id, boolean admin) {
		// Construct a new application for the caller
		try {
			DynamixApplication app = new DynamixApplication(type, id, plug.getName());
			app.setAppDescription(plug.getDescription());
			app.setAdmin(admin);
			return app;
		} catch (MalformedURLException e) {
			Log.w(TAG, "Could not create application for id " + id);
		}
		return null;
	}

	@Override
	public boolean equals(Object candidate) {
		// first determine if they are the same object reference
		if (this == candidate)
			return true;
		// make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		InternalFacadeBinder other = (InternalFacadeBinder) candidate;
		return this.appId.equalsIgnoreCase(other.appId);
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.appId.hashCode();
		return result;
	}
}