/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.update.contextplugin;

import java.io.Serializable;

import org.ambientdynamix.api.contextplugin.ContextPlugin;
import org.ambientdynamix.api.contextplugin.PluginConstants.UpdatePriority;

/**
 * Provides information regarding a pending (discovered but not installed) ContextPlugin.
 * 
 * @author Darren Carlson
 */
public class PendingContextPlugin implements Serializable {
	private static final long serialVersionUID = -3008156585439407905L;
	// Private data
	private ContextPlugin pendingPlugin;
	private String updateMessage;
	private UpdatePriority priority = UpdatePriority.NORMAL;
	private boolean hasError = false;
	private String errorMessage = "";
	private boolean update;
	private ContextPlugin updateTargetPlugin;

	/**
	 * Creates a new PendingContextPlugin with no message and UpdatePriority.NORMAL.
	 * 
	 * @param pendingPlugin
	 *            The newly discovered plug-in.
	 */
	public PendingContextPlugin(ContextPlugin pendingPlugin) {
		this.pendingPlugin = pendingPlugin;
	}

	/**
	 * Creates a new PendingContextPlugin with an update message.
	 * 
	 * @param pendingPlugin
	 *            The newly discovered plug-in.
	 * @param updateMessage
	 *            An associated message.
	 * @param priority
	 *            An associated UpdatePriority.
	 */
	public PendingContextPlugin(ContextPlugin pendingPlugin, String updateMessage, UpdatePriority priority) {
		this.pendingPlugin = pendingPlugin;
		this.priority = priority;
		this.updateMessage = updateMessage;
	}

	/**
	 * Creates an update PendingContextPlugin for the specified updateTargetPlugin (i.e., the pendingPlugin is used to
	 * update the updateTargetPlugin) with no message and UpdatePriority.NORMAL.
	 * 
	 * @param pendingPlugin
	 *            The pending Plug-in update.
	 * @param updateTargetPlugin
	 *            The Plug-in to be updated.
	 */
	public PendingContextPlugin(ContextPlugin pendingPlugin, ContextPlugin updateTarget) {
		this(pendingPlugin);
		this.update = true;
		this.updateTargetPlugin = updateTarget;
	}

	/**
	 * Creates an update PendingContextPlugin for the specified updateTargetPlugin (i.e., the pendingPlugin is used to
	 * update the updateTargetPlugin), including an update message and update priority.
	 * 
	 * @param pendingPlugin
	 *            The pending Plug-in update.
	 * @param updateTargetPlugin
	 *            The Plug-in to be updated.
	 * @param updateMessage
	 *            An associated message.
	 * @param priority
	 *            An associated UpdatePriority.
	 */
	public PendingContextPlugin(ContextPlugin pendingPlugin, ContextPlugin updateTarget, String updateMessage,
			UpdatePriority priority) {
		this(pendingPlugin, updateMessage, priority);
		this.update = true;
		this.updateTargetPlugin = updateTarget;
	}

	/**
	 * Creates a PendingContextPlugin with an error state.
	 * 
	 * @param errorMessage
	 *            The error message.
	 */
	public PendingContextPlugin(String errorMessage) {
		this.hasError = true;
		this.errorMessage = errorMessage;
	}

	/**
	 * Returns the discovered ContextPlugin.
	 */
	public ContextPlugin getPendingContextPlugin() {
		return pendingPlugin;
	}

	/**
	 * Returns the error message associated with this PendingContextPlugin.
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Returns the update message associated with the PendingContextPlugin.
	 */
	public String getUpdateMessage() {
		return updateMessage;
	}

	/**
	 * Returns the UpdatePriority associated with the PendingContextPlugin.
	 */
	public UpdatePriority getPriority() {
		return priority;
	}

	/**
	 * Returns true if this PendingContextPlugin encountered an error; false otherwise.
	 */
	public boolean hasError() {
		return hasError;
	}

	/**
	 * Sets the ContextPlugin.
	 */
	public void setContextPlugin(ContextPlugin plug) {
		this.pendingPlugin = plug;
	}

	/**
	 * Sets the message associated with the PendingContextPlugin.
	 */
	public void setMessage(String updateMessage) {
		this.updateMessage = updateMessage;
	}

	/**
	 * Sets the UpdatePriority associated with the PendingContextPlugin.
	 */
	public void setPriority(UpdatePriority priority) {
		this.priority = priority;
	}

	/**
	 * Returns true if this PendingContextPlugin is an update (i.e., targets a plug-in for updating); false otherwise.
	 */
	public boolean isUpdate() {
		return this.update;
	}

	/**
	 * Returns the ContextPlugin target to be updated or null if this is not an update.
	 */
	public ContextPlugin getUpdateTargetPlugin() {
		return this.updateTargetPlugin;
	}

	/**
	 * Sets the ContextPlugin target to be updated. If the incomging plug-in is not null, update is set to true,
	 * otherwise it's set to false.
	 */
	public void setUpdateTarget(ContextPlugin updateTarget) {
		if (updateTarget != null) {
			this.updateTargetPlugin = updateTarget;
			this.update = true;
		} else {
			this.update = false;
		}
	}

	@Override
	public String toString() {
		return "PendingContextPlugin for: " + pendingPlugin;
	}

	@Override
	public int hashCode() {
		if (update)
			return pendingPlugin.hashCode() + updateTargetPlugin.hashCode();
		else
			return pendingPlugin.hashCode();
	}

	@Override
	public boolean equals(Object candidate) {
		// first determine if they are the same object reference
		if (this == candidate)
			return true;
		// make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Cast the incoming candidate to a PendingContextPlugin
		PendingContextPlugin other = (PendingContextPlugin) candidate;
		// Check if the candidate has an update
		if (update) {
			if (!other.isUpdate()) {
				return false;
			} else {
				// Check if BOTH the pendingPlugins and currentPlugins are the same.
				return this.pendingPlugin.equals(other.getPendingContextPlugin())
						&& this.updateTargetPlugin.equals(other.getUpdateTargetPlugin());
			}
		} else {
			// Check if the pendingPlugins are the same.
			return this.pendingPlugin.equals(other.getPendingContextPlugin());
		}
	}
}