/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.api.contextplugin;

import java.io.FileDescriptor;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

/**
 * IBinder implementation that enables various classes (e.g., the WebListener) to be properly identified as a
 * IDynamixListener by the Dynamix Framework.
 * 
 * @author Darren Carlson
 * 
 */
public class SimpleBinder implements IBinder {
	private static String TAG = SimpleBinder.class.getSimpleName();
	private String id;

	public SimpleBinder(String id) {
		this.id = id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object candidate) {
		// First determine if they are the same object reference
		if (this == candidate)
			return true;
		// Make sure they are the same class
		if (candidate == null || candidate.getClass() != getClass())
			return false;
		// Ok, they are the same class... check if their id's are the same
		SimpleBinder other = (SimpleBinder) candidate;
		return this.id.equalsIgnoreCase(other.id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + this.id.hashCode();
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dump(FileDescriptor arg0, String[] arg1) throws RemoteException {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInterfaceDescriptor() throws RemoteException {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isBinderAlive() {
		// Return true, since web app liveliness is determined by the WebConnector.
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void linkToDeath(DeathRecipient arg0, int arg1) throws RemoteException {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean pingBinder() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IInterface queryLocalInterface(String arg0) {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean transact(int arg0, Parcel arg1, Parcel arg2, int arg3) throws RemoteException {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unlinkToDeath(DeathRecipient arg0, int arg1) {
		return false;
	}
}